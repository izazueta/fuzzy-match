import sys
from fuzzywuzzy import process

entrada = [line.rstrip('\n') for line in open(sys.argv[1])]
comparar = [line.rstrip('\n') for line in open(sys.argv[2])]

print("ENTRADA, MATCH, SCORE")

for linea in entrada:
    match = process.extractOne(linea, comparar)
    print( linea +", "+ match[0] +", "+ str(match[1]))
